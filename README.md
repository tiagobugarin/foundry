# Facilitando o FoundryVTT com Docker

Tentando otimizar e sistematizar a instalação, manutenção e gestão do FoundryVTT com Docker

# Passo a passo

Clonar este projeto base, entrar no diretório e criar a estrutura necessária:
```
git clone https://gitlab.com/tiagobugarin/foundry.git && cd foundry && mkdir {app,data}
```

Obter o zip do FoundryVTT e descomprimir o zip no diretório destinado a ele:
> ATENÇÃO: substituir `<download url>` pelo link de download obtido no painel da loja do FoundryVTT!
```
wget -qO foundryvtt.zip <download url> && unzip -q foundryvtt.zip -d app || echo "Houve um problema no download, verifique o link ou suba via SCP"
```

Remover versões antigas do Docker, instalar o Docker dos repositórios oficiais do projeto, instalar o plugin `compose`, atribuir o grupo `docker` ao usuário atual e "ativar" esse grupo ainda nesta sessão:
```
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
curl -fsSL https://get.docker.com | bash
newgrp docker
```

Se pretende utilizar o serviço de backup local e/ou acessar o Foundry através de (sub)domínio é preciso copiar o arquivo `env-sample` como `.env` e editá-lo:
```
cp env-sample .env
nano .env
```

Criar imagem Docker com NodeJS e criar os containers para executar o FoundryVTT e um servidor web (NginX) como proxy:
```
docker compose build
docker compose up -d
```

Neste momento já deve ser possível acessar o serviço.  
Os passos a seguir são recomendados (*) ou opcionais (~).

(~) Trocar senha do usuário:
```
sudo passwd $USER
```

(~) Adicionar chaves públicas de outros usuários:
```
nano ~/.ssh/authorized_keys
```

(*) Mudar o fuzo horário para São Paulo (outros são possíveis):
```
sudo timedatectl set-timezone America/Sao_Paulo
```

(*) E, finalmente, reiniciar a máquina pra verificar se tudo está correto:
```
sudo reboot
```

# Créditos

Imagem dado de Alexis Bailey sob [Creative Commons (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/) (https://www.freepngimg.com/png/81025-art-dice-dungeons-system-dragons-d20-triangle)
