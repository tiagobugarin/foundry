#!/bin/bash

# A script to perform incremental backups using rsync
# https://linuxconfig.org/how-to-create-incremental-backups-using-rsync-on-linux

set -o errexit
set -o nounset
set -o pipefail

readonly SOURCE_DIR="/foundry"
readonly BACKUP_DIR="/backup"
readonly DATETIME="$(date '+%Y-%m-%d_%H:%M:%S')"
readonly BACKUP_PATH="${BACKUP_DIR}/${DATETIME}"
readonly LATEST_LINK="${BACKUP_DIR}/latest"

mkdir -p "${BACKUP_DIR}"

rsync -av --delete \
  "${SOURCE_DIR}/" \
  --link-dest "${LATEST_LINK}" \
  --exclude=".cache" \
  "${BACKUP_PATH}"

rm -rf "${LATEST_LINK}"
cd "${BACKUP_DIR}"
ln -s "${DATETIME}" "${LATEST_LINK}"
